.SUFFIXES: .fig .eps .tex .dvi .ps .pdf

SHELLCODE = device.sh all
SQLCODE = all.sql equip.sql login.sql timing.sql
FIGS = bus.fig
FIGURES = bus.eps converter-ccdb-data.eps ccdb-timing.eps
PDFVIEWER = evince
PDFVIEWER = acroread
TARGET = recipe

.tex.pdf:
	pdflatex $* && pdflatex $*

.fig.eps:
	fig2dev -L eps $*.fig $*.eps

all: $(TARGET).pdf
view: $(TARGET).pdf
	$(PDFVIEWER) $(TARGET).pdf
$(TARGET).pdf: $(FIGURES)

clean:
	rm -f *.dvi *.aux *.log *.pdf
