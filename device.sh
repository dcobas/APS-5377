#!	/bin/bash

HOSTNAME=$1

sqlplus -S $(cat oracle_copub_credentials) <<EOF

set line 130
set pagesize 0
set wrap off
set feedback off

select devicename, fecname, cr.bus_loop, cr.module_crate
from devices d inner join computers co
   on co.computer_id = d.computer_id
    inner join crates cr
     on cr.compname = co.compname and
        cr.cratelabel = d.devicename
where devicename = '$HOSTNAME'
/
EOF
