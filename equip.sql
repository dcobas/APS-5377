select devicename, fecname, cr.bus_loop, cr.module_crate
from devices d inner join computers co
   on co.computer_id = d.computer_id
    inner join crates cr
     on cr.compname = co.compname and
        cr.cratelabel = d.devicename
where devicename = 'DX.SBP-CT'
