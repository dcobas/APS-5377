select classname, fecname, devicename, ptim, eqname2 as converter, cratelabel, bus_loop as bc, module_crate as rti from (
  select * from (
    select d.classname, d.fecname, d.devicename, device_field_value as ptim from fesa_device_field_values_v f
    join devices_lite_v d on f.devicename = d.devicename
    where d.classname = 'LTIM'
    and field_name = 'eqpNb'
    union all
    select 'CTIM', null, event_name, null from hl_events_v
  ) t
  left join equiprelations_v e on eqname1 = t.devicename
  left join crates cr on cr.compname = fecname
  where equiprelation = 'TRIG-ACQUISITION'
) where eqname2 = 'L4L.QFA03070';
