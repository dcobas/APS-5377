select f.devicename as acq_trigger, f.classname, f.fecname as timing_fec, f.device_field_value as PTIM
from fesa_device_field_values_v f
where f.devicename = 'IX.A-PPOW'
      and f.classname = 'LTIM'
      and f.field_name = 'eqpNb'
