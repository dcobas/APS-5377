\documentclass[11pt]{article}
\usepackage{palatino}
\usepackage{graphicx}
\usepackage{paralist}
\usepackage[margin=1in]{geometry}

\title{How to compute timing limits
	for a 1553-controlled power converter}
\author{Juan David Gonz\'alez Cobas\\
	BE-CO-HT}

\begin{document}
\maketitle

\begin{abstract}
It often happens that power converters controlled by PowM1553
sporadically miss acquisitions. The usual root cause of this behaviour
is bad configuration of acquisition timing limits. These are hard to get
right, because the acquisition latency of a particular power converter
is unknown in general, thus limits are computed just by trial and error.

This document explains how to determine a converter's acquisition
latency in a more systematic manner.  With this information, timings for
control and acquisition can be computed reliably so that guesswork is
not needed anymore.
\end{abstract}

\section{Background: power converter timing}

It is useful to keep in mind the following picture on how the
MIL1553-controlled converters work (see figure~\ref{bus}).

Acquisition and CCV
controls operate independently to all effects: thus, for the purpose of
the present discussion, we may safely ignore the CCV control process.
\begin{figure}
\includegraphics[width=0.9\textwidth]{bus.eps}
\caption{MIL1553 control of power converters}
\label{bus}
\end{figure}

In most (but not all) cases, power converters have their acquisition
triggered by a timing pulse input to the converter itself. It takes some
time for the converter ADC to produce an AQN value (this is a major
contribution to the \emph{latency} that we want to measure). Eventually,
the value is written into the \emph{transmission buffer} of the RTI
controller that links the equipment to the MIL1553 bus, and its TB
(transmission buffer busy) flag is set.

This TB flag is the mechanism whereby the FESA class PowM1553 that runs
in the controlling FEC will know that an acquisition is ready. PowM1553
runs through the sequence of controlled equipment polling the TB flag of
each; if TB is 0, an ``acquisition not ready'' diagnostic is produced.
On the other hand, if TB is 1, the acquisition is retrieved  and the
polling moves on to the next equipment.

In most cases, failure to retrieve acquisitions is due to PowM1553
polling some equipment \emph{too early}, i.e., \emph{before} the
converter had the time to do the A/D conversion of current value.
Therefore, a means to measure the latency of the converter ADC is
necessary to appropriately set the timing.

\section{A recipe for computing converter latencies}

This section illustrates with an example the procedure to accurately
determine a converter latency period. Most of the procedure can be
automated; however, there are so many exceptional cases that no claim
can be made as to exhaustivity.

Let us consider a particular converter and trace the necessary steps to
compute its acquisition latency. Consider ITH.QDN10. The latency
computation will require that we assemble some information about this
device:
\begin{compactitem}
\item controlling FEC
\item MIL1553 bus number and RTI number
\item timing that triggers the acquisition
\item the FEC that generates that timing (if the trigger is an LTIM), or
     any FEC that receives that timing event (if the trigger is a CTIM).
\end{compactitem}

This information can be collected manually from the declarations
concerning ITH.QDN10, or, in slightly less reliable manner, by querying
the CCDB to automate the task. We will show first how to visually locate
the metadata through the CCDB Data Browser; then, a semi-automated
routine will be explained in section\ref{automation}.

\begin{figure}
\includegraphics[width=0.95\textwidth]{converter-ccdb-data.eps}
\caption{CCDB information on example converter ITH.QDN10}
\label{converter-data}
\end{figure}

Figure~\ref{converter-data} shows the CCDB Data Browser information on
ITH.QDN10. What concerns us here is underlined in red:
\begin{compactitem}
\item equipment name (\verb|ITH.QDN10|)
\item controlling FEC (\verb|cfc-351-rpow|)
\item MIL1553 bus/rti address(bus 2, rti 7)
\item timing triggering acquisition (\verb|IX.A-PPOW|)
\end{compactitem}
This information is already enough to probe into the controlling FEC and
determining the moments at which the TB flag of the RTI associated to
the converter raises to 1, signalling the deposition of an AQN value in
the converter. This information is given by the
\verb|rti_poll_flags|
utility. For example:
\begin{verbatim}
cs-ccr-dev4:recipe$ ssh cfc-351-rpow rti_poll_flags -b 2 -r 7 -p 5000 -n 2000 -o /tmp/tb-tags
cs-ccr-dev4:recipe$ ssh cfc-351-rpow rti_poll_flags_decode /tmp/tb-tags | head -n 20
   1490258466.785758         09:41:06.785758  0  0
[...]
   1490258467.885066         09:41:07.885066  0  0
   1490258467.890066         09:41:07.890066  0  0
   1490258467.895066         09:41:07.895066  1  0
   1490258467.900066         09:41:07.900066  1  0
   1490258467.905066         09:41:07.905066  1  0
   1490258467.910066         09:41:07.910066  1  0
   1490258467.915066         09:41:07.915066  1  0
   1490258467.920066         09:41:07.920066  1  0
   1490258467.925066         09:41:07.925066  1  0
   1490258467.930056         09:41:07.930056  1  0

\end{verbatim}
This produces values of TB and RB flags in the RTI of our converter
at 5ms intervals during 10 seconds. The third output
column is the status of the TB flag at the timestamp; the flipping
of TB from 0 to 1 at time 1490258467.895066 marks the end of A/D
conversion by the ADC of the converter. The timestamps of these events
(TB rising edge) need to be collected.

We need to correlate these timestamps with the timings that triggered the
acquisition. Collecting these is trickier, because the triggering may be
a pulse sent out from a different FEC than the one running PowM1553,
an event usually controlled by an LTIM class.
This is the case with our example, as can be seen in the CCDB
Data Browser page describing IX.A-PPOW. Figure~\ref{timing-ccdb}
shows where to find the relevant fields:
\begin{compactitem}
\item timing name (\verb|IX.A-PPOW|)
\item timing type (LTIM)
\item FEC generating the timing (\verb|cfv-351-ctim|)
\item number of the PTIM (as \verb|eqpNb| field, number 10033)
\end{compactitem}

\begin{figure}
\includegraphics[width=0.95\textwidth]{ccdb-timing.eps}
\caption{CCDB information on timing IX.A-PPOW}
\label{timing-ccdb}
\end{figure}

As with the TB flags, these metadata allow us to tap directly into the
FEC running the LTIM and gather timestamps contemporary to the period
sampled in the 1553 bus. There are not many ways to do this
programmatically, but, for ease of understanding, we'll collect the
output of \verb|ctrtest| (abbreviated here to fit the page width):
\begin{verbatim}
cs-ccr-dev4:recipe$ echo '20(wi p 10033) q' |  ssh cfv-351-ctim ctrtest                                                                                                                       
ctrtest: See <news> command
[......]
[...] PTIM[10033:RtAqn:IX.A-PPOW] [...] Time[Thu-23/Mar/2017 10:40:43.8710000000] [...]
[...] PTIM[10033:RtAqn:IX.A-PPOW] [...] Time[Thu-23/Mar/2017 10:40:45.0710000000] [...]
[...] PTIM[10033:RtAqn:IX.A-PPOW] [...] Time[Thu-23/Mar/2017 10:40:46.2710000000] [...]
[...] PTIM[10033:RtAqn:IX.A-PPOW] [...] Time[Thu-23/Mar/2017 10:40:47.4710000000] [...]
[...] PTIM[10033:RtAqn:IX.A-PPOW] [...] Time[Thu-23/Mar/2017 10:40:48.6710000000] [...]
[......]
\end{verbatim}
Although this is admittedly the most hackerish way to collect data,
it's straightforward to parse this into timestamps that can be
correlated to the times found for TBs. More orthodox approaches are left
as an exercise. With some trivial but boring data processing, which we
hide here behind the \verb|process-timestamps| script, to be described
in the next section, we see
\begin{verbatim}
cs-ccr-dev4:recipe$ python process-timestamps 
1490262223.87100 1490262223.89507  0.0241
1490262225.07100 1490262225.09507  0.0241
1490262226.27100 1490262226.29507  0.0241
1490262227.47100 1490262227.49507  0.0241
1490262228.67100 1490262228.70507  0.0341
1490262229.87100 1490262229.89507  0.0241
1490262231.07100 1490262231.09507  0.0241
1490262232.27100 1490262232.29507  0.0241
\end{verbatim}
that the latency is absolutely regular (within the 5ms precision of our
sampling process): all TB timestamps follow the timing timestamps after
less than 25ms.

In a nutshell, given a power converter, its AQN latency can be computed
by means of the following recipe:
\begin{enumerate}
\item gather the required data: controlling MIL1553 FEC, bus controller
number and RTI number, plus the timing that triggers the acquisition. If
the latter is an LTIM, gather its server FEC and the equipment number of
the LTIM; if it is a CTIM, any FEC in the same timing domain will do,
only that the CTIM number is required.
\item use \verb|poll_rti_flags| to sample the controlling RTI TB flag
during around 20s.
\item simultaneously to this, gather timestamps of the triggering timing
by using ctrtest or hooking to the timing using RDA.
\item correlate the timing and TB timings to find the acquistion delay.
\end{enumerate}

\section{Automating the recipe}
\label{automation}

The recipe described in the preceding section can be performed manually
for instructional purposes, but in practice some automation is required.
We introduce here some tools that alleviate the pain in the above
data-tracking process. Keep in mind, however, that a substantial
proportion of converters in the field are ``exceptional'' in one way or
another (if only because of anomalous CCDB declarations), which makes
full automation extremely difficult.

\subsection{Collection of converter metadata}
Coordinates of a converter (1553 coordinates plus AQN trigger timing)
can be retrieved by querying the CCDB.  A script \verb|convdata| is
furnished that avoids the tedious readouts from the Data Browser by
invoking the \verb|convdata.sql| query. Here is a sample run of
\verb|convdata| on our example power converter:
\begin{verbatim}
cs-ccr-dev4:recipe$ python convdata ITH.QDN10
CONVERTER            ITH.QDN10       
FEC                  cfc-351-rpow    
BC                   2               
RTI                  7               
ACQ_TIMING           IX.A-PPOW       
EQUIPRELATION        TRIG-ACQUISITION
CLASSNAME            LTIM            
TIMING_FEC           cfv-351-ctim    
PTIM                 10033           

# capture acquisition trigger timings by:
echo 'wi p 10033 30(wi) q' |
ssh cfv-351-ctim ctrtest |
tee tim-tags

# capture TB flag evolution by:
ssh cfc-351-rpow rti_poll_flags -b 2 -r 7 -o /tmp/tb-flags -p 5000 -n 6000
ssh cfc-351-rpow rti_poll_flags_decode /tmp/tb-flags > tb-tags
\end{verbatim}
Together with the metadata required to compute latency, a set
of shell commands is suggested that will gather the timestamps for
acquisition and timing in files \verb|tb-tags| and \verb|ts-tags|,
respectively.

\subsection{Processing timestamp data}

Parsing the gathered information and correlating timestamps for trigger and
end-of-acquisition is a tedious process that, fortunately, can be
completely automated. The script \verb|process-timestamps| does
precisely that:
\begin{verbatim}
cs-ccr-dev4:recipe$ python process-timestamps                                                                                                                                                 
1490262223.87100 1490262223.89507  0.0241
1490262225.07100 1490262225.09507  0.0241
1490262226.27100 1490262226.29507  0.0241
1490262227.47100 1490262227.49507  0.0241
1490262228.67100 1490262228.70507  0.0341
1490262229.87100 1490262229.89507  0.0241
1490262231.07100 1490262231.09507  0.0241
1490262232.27100 1490262232.29507  0.0241
\end{verbatim}
From this, it can be inferred that the latency of ITH.QDN10 is likely to
be 25ms (the 34.1ms outlier might be for real, or, much likelier, an
artifact of the real-time OS behaviour in \verb|cfc-351-rpow|,
compounded with the 5ms we chose as sampling granularity).

\end{document}

