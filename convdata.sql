select d.devicename as converter,
    co.compname as fec,
    cr.bus_loop as bc,
    cr.module_crate as rti,
    tim.devicename as acq_timing,
    rel.equiprelation,
    t.classname, t.timing_fec, t.ptim
from equiprelations_v rel inner join devices_v tim
	on rel.equip1 = tim.device_id
    inner join devices_v d
	on rel.equip2 = d.device_id
    inner join computers_v co
	on co.computer_id = d.computer_id
    inner join crates_v cr
	on cr.compname = co.compname and
	   cr.cratelabel = d.devicename
    inner join (select f.devicename as acq_trigger, f.classname, f.fecname as timing_fec, f.device_field_value as PTIM
	from fesa3_device_field_values_v f
	where f.classname = 'LTIM' and f.field_name = 'eqpNb'
    ) t
    	on t.acq_trigger = tim.devicename
where  rel.equiprelation = 'TRIG-ACQUISITION'
    and d.devicename = :conv
